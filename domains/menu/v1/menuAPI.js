const express = require('express');
const menuController = require('./menuController');
const menuValidation = require('./menuValidation');
const validation = require('../../../middlewares/validation');

const router = express.Router();

router.get(
    '/',
    menuController.index
);

router.post(
    '/',
    validation(menuValidation.create),
    menuController.create
);

router.get(
    '/:id',
    menuController.detail
);

router.put(
    '/:id',
    validation(menuValidation.updateOne),
    menuController.updateOne
);

router.delete(
    '/:id',
    menuController.deleteOne
);

module.exports = router;
