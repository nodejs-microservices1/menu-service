const {Menu} = require('../menu');

/**
 * Get Total Data
 * @param {Array} filters
 */
const total = async (filters = [{}]) => {
    // init aggregate pipelines
    let pipelines = [];

    // filters
    pipelines.push({$match: {$and: filters}});

    // count
    pipelines.push({$count: 'total'});

    // result
    let result = await Menu.aggregate(pipelines);

    if (result && result.length > 0) {
        return result[0];
    }

    return {total: 0};
};

/**
 * Get List Data
 * @param {Array} filters
 * @param {int} page
 * @param {int} limit
 * @param {Object} sort
 */
const list = async (filters = [{}], page = 0, limit = 0, sort) => {
    // init aggregate pipelines
    let pipelines = [];

    // filters
    pipelines.push({$match: {$and: filters}});

    // sort
    if (sort && sort !== '') {
        pipelines.push({$sort: sort});
    }

    // pagination
    let pageVal = parseInt(page);
    let limitVal = parseInt(limit);
    if (pageVal > 0 && limitVal > 0) {
        let skip = (pageVal - 1) * limitVal;
        pipelines.push({$limit: skip + limitVal}, {$skip: skip});
    }

    // result
    return Menu.aggregate(pipelines);
};

/**
 * Find By ID
 * @param {String} id
 */
const findById = async (id) => {
    return Menu.findOne({_id: id}).lean();
};

/**
 * Create New Data
 * @param {Object} data
 */
const create = async (data) => {
    let menu = new Menu(data);
    return menu.save();
};

/**
 * Update One Data with filter ID
 * @param {String} id
 * @param {Object} data
 */
const updateOne = async (id, data) => {
    return Menu.findOneAndUpdate({_id: id}, data, {
        returnOriginal: false,
    });
};

/**
 * Delete One Data with filter ID
 * @param {String} id
 */
const deleteOne = async (id) => {
    return Menu.deleteOne({_id: id});
};

module.exports = {
    total,
    list,
    findById,
    create,
    updateOne,
    deleteOne,
};
