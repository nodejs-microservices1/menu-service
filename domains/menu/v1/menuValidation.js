const Joi = require('joi');

const create = Joi.object({
    name: Joi.string().required(),
    sku: Joi.string().required(),
    price: Joi.number().required(),
    discount: Joi.number().required(),
    stock: Joi.number().required(),
});

const updateOne = Joi.object({
    name: Joi.string().required(),
    sku: Joi.string().required(),
    price: Joi.number().required(),
    discount: Joi.number().required(),
    stock: Joi.number().required(),
});

module.exports = {
    create,
    updateOne,
};
