const menuDAL = require('./menuDAL');
const errorHelper = require('../../../libraries/error');
const mongoQuery = require('../../../libraries/mongoQuery');

/**
 * Get List Data
 * @param {Object} query values for filtering needs
 */
const index = async (query) => {
    // get query params
    const {page, limit, search, sort_by, sort_type} = query;

    // init filters
    let filters = [{}];

    // filter search
    if (search && search !== '') {
        filters.push({
            $or: [
                {name: mongoQuery.searchLike(search)},
                {sku: mongoQuery.searchLike(search)},
            ],
        });
    }

    // sort
    const sort = mongoQuery.sortBy(sort_by, 'createdAt', sort_type, 'asc');

    // get data
    const menus = await menuDAL.list(filters, page, limit, sort);
    const totalFiltered = await menuDAL.total(filters);

    // response
    return {
        data: menus,
        meta: {
            total_filtered: totalFiltered.total,
        },
    };
};

/**
 * Create Data
 * @param {Object} body
 */
const create = async (body) => {
    let result = await menuDAL.create(body);
    if (!result) errorHelper.throwInternalServerError("Create Failed");

    return result;
};

/**
 * Get Detail Data
 * @param {String} id
 */
const detail = async (id) => {
    const menu = await menuDAL.findById(id);
    if (!menu) errorHelper.throwNotFound();
    return menu;
};

/**
 * Update Data
 * @param {String} id
 * @param {Object} body
 */
const updateOne = async (id, body) => {
    const menu = await menuDAL.findById(id);
    if (!menu) errorHelper.throwNotFound();

    let result = await menuDAL.updateOne(id, body);
    if (!result) errorHelper.throwInternalServerError("Update Failed");

    return result;
};

/**
 * Delete Data
 * @param {String} id
 */
const deleteOne = async (id) => {
    const menu = await menuDAL.findById(id);
    if (!menu) errorHelper.throwNotFound();

    let result = await menuDAL.deleteOne(id);
    if (!result) errorHelper.throwInternalServerError("Delete Failed");

    return result;
};


module.exports = {
    index,
    create,
    detail,
    updateOne,
    deleteOne,
};
