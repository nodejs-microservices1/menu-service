const {Schema, model, SchemaTypes: {ObjectId}} = require('mongoose');

let menuSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true
        },
        sku: {
            type: String,
            required: true,
            unique: true
        },
        price: {
            type: Number,
            required: true,
        },
        discount: {
            type: Number,
            required: true,
        },
        stock: {
            type: Number,
            required: true,
        },
    },
    {timestamps: true}
);

exports.Menu = model('Menu', menuSchema);
